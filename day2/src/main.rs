use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<dyn Error>> {
    let (x, y, _) = read_to_string("input.txt")?
        .lines()
        .map(
            |w| match w.split_ascii_whitespace().collect::<Vec<_>>()[..] {
                ["forward", n] => (n.parse::<i64>().unwrap(), 0),
                ["down", n] => (0, n.parse::<i64>().unwrap()),
                ["up", n] => (0, -n.parse::<i64>().unwrap()),
                _ => panic!("Unexpected input"),
            },
        )
        .fold((0, 0, 0), |(x, y, aim), (x_inc, aim_inc)| {
            (x + x_inc, y + x_inc * aim, aim + aim_inc)
        });
    println!("{}", x * y);

    Ok(())
}
