use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<dyn Error>> {
    let (output, _, _, _) = read_to_string("input.txt")?
        .lines()
        .map(|l| l.parse().unwrap())
        .fold(
            (0, 0, 0, u64::MAX / 2),
            |(acc, first, second, third), fourth| {
                (
                    acc + (second + third + fourth > first + second + third) as u64,
                    second,
                    third,
                    fourth,
                )
            },
        );
    println!("{}", output);

    Ok(())
}
